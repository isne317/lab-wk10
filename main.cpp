#include "Graph.h"
#include <iostream>
#include <list>
#include <string>

using namespace std;

int main(void) {
    list<int> ArrayList;
    Graph gappy;

    // Dimensions
    int node;
    cout << "How many nodes: ";
    cin >> node;
    cout << endl;
    // Dynamic allocation
    int row = node, col = node;
    int** ary = new int*[row];
    for (int i = 0; i < row; ++i) {
        ary[i] = new int[col];
    }

    // Input
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            cout << "Input matrix[" << i << "][" << j << "]: ";
            cin >> ary[i][j];
        }
    }
    cout << "--------------------------------";
    // Output
    cout << "\nAdjacent Matrix of this graph is: " << endl << endl;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < col; ++j) {
            cout << ary[i][j] << " ";
            if (j == col - 1) {
                cout << endl;
            }
        }
    }
    // Push value to the list
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            ArrayList.push_back(ary[i][j]);
        }
    }
    // Sending array with size
    gappy.pushInList(node, ArrayList);
    cout << "--------------------------------";
    cout << "\nAdjacency List" << endl << endl;

    gappy.showList(node);
    cout << endl << "--------------------------------";

    gappy.Dijkstra(node, ary);
}
