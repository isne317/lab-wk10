#include <iostream>
#include <list>
using namespace std;

class Node {
    public:
        Node() { edge = vert = NULL; }
        Node(const int& el, Node *a = 0, Node *b = 0){ key = el; edge = a; vert = b; }
        Node *edge, *vert;
        int key;
};

class  Graph {
    public:
        Graph() { root = 0; }
        ~Graph(){ clear(); };
        void clear() { clear(root); root = 0; }
        void pushInList(int nodeSize, list<int> aryList);
        void showList(int nodeSize);    
        bool Multig(int nodeSize);      
        bool Pseudog(int nodeSize);     
        bool Directg(int nodeSize);     
        bool Weightg(int nodeSize);     
        bool Completeg(int nodeSize);   
        
        void Dijkstra(int nodeSize, int** ary); //Dijkstra's Algorithm

    private:
        void clear(Node *p);
        Node *root;
};

void Graph::clear(Node *p){
	if (p != 0){clear(p->edge); clear(p->vert); delete p;}
}

void Graph::pushInList(int nodeSize,list<int> aryList){
	Node *index,*p;
	index = new Node();
	root = index;
	for(int i = 0; i < nodeSize; i++){
		index->key = i+1;
		p = index;
		for(int j = 0; j < nodeSize; j++){
			if(aryList.front()!=0) {
				Node *Edge,*Vertice;
				Edge = new Node();
				Vertice = new Node();
				p->edge = Edge;
				Edge->key = j+1;
				Vertice->key = aryList.front();
				Edge->vert = Vertice;
				p = p->edge;
			}

			aryList.pop_front();
		}
		Node *tmp;
		tmp = new Node();
		index->vert = tmp;
		index = tmp;
	}
}

void Graph::showList(int nodeSize){
	Node *tmp = root;
	char index;
	for(int i = 0; i < nodeSize; i++){
		index = i+65;
		cout<<index;
		Node *p = tmp->edge,*vt;
		index = tmp->key+64;
		vt = p->vert;
		while(vt != NULL){
			index = p->key+64;
			cout<<" -> "<<index<<vt->key<<" ";
			vt = vt->vert;
			if(vt == NULL && p->edge != NULL){
				p = p->edge;
				vt = p->vert;
			}
		}
		tmp = tmp->vert;
		if(i != nodeSize-1) {cout<<"\n|\n";}
	}
}

bool Graph::Multig(int nodeSize){
	Node *tmp = root,*p,*chk;
	int counter = 0;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		chk = p->vert;
		while(chk != NULL){
			counter++;
			chk = chk->vert;
			if(counter>1){return true;}
            else if(chk == NULL && p->edge != NULL){
				p = p->edge;
				chk = p->vert;
				counter = 0;
			}
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Pseudog(int nodeSize){
	Node *tmp = root,*p;
	char index;
	while(tmp != NULL){
		p = tmp->edge;
		while(p != NULL){
			if(p->key == tmp->key){index = 64+p->key; return true;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Directg(int nodeSize){
	Node *tmp = root,*p,*chk;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		if(p->key != tmp->key){
			chk = root;
			for(int j = 1; j < p->key; j++) {chk = chk->vert;}
			for(int j = 0; j < tmp->key; j++) {chk = chk->edge;}
			if(chk->vert->key != p->vert->key) {return true;}
		}
	}
	return false;
}

bool Graph::Weightg(int nodeSize){
	Node *tmp = root,*p;
	while(tmp != NULL){
		p = tmp->edge;
		while(p != NULL){
			if(p->vert->key == -1){return true;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Completeg(int nodeSize){
	Node *tmp = root,*p;
	int counter = 0;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		while(p != NULL){
			if(p->key != tmp->key){counter++;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	if(counter == nodeSize*(nodeSize-1))return true; else return false;
}

void Graph::Dijkstra(int nodeSize, int** ary){
	int distance[nodeSize];
	int src = 0;
    bool visited[nodeSize];

    for (int i = 0; i < nodeSize; i++){
    	distance[i] = INT_MAX;
		visited[i] = false;
	}	//cleared
	
    distance[src] = 0;
    for (int count = 0; count < nodeSize-1; count++){
        int min = INT_MAX, min_index;
        for (int v = 0; v < nodeSize; v++){
        	if (visited[v] == false && distance[v] <= min){
        		min = distance[v];
				min_index = v;
			}
		}
        int x = min_index;
        visited[x] = true;
        for (int y = 0; y < nodeSize; y++){
        	if (!visited[y] && ary[x][y] && distance[x] != INT_MAX && distance[x]+ary[x][y] < distance[y]){
        		distance[y] = distance[x] + ary[x][y];
			}
		}    
    }
    
    cout << endl << "Dijkstra's Algorithm" << endl;
    for (int i = 0; i < nodeSize; i++){
    	cout << distance[i] << endl;
	}
}
#endif
